<?php
namespace Avannubo\Slider\Seeds;
use Illuminate\Database\Seeder;
use App\Permission;
class PermissionSliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $person = new Permission([
            'name' => 'slider.view',
            'display_name' => 'slider  view',
            'description' => 'Ver slider'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'slider.create',
            'display_name' => 'slider create',
            'description' => 'Crear slider'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'slider.edit',
            'display_name' => 'slider edit',
            'description' => 'Editar slider'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'slider.delete',
            'display_name' => 'slider delete',
            'description' => 'Eliminar slider'
        ]);
        $person->save();
    }
}
