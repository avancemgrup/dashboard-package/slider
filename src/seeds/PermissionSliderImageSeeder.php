<?php
namespace Avannubo\Slider\Seeds;
use Illuminate\Database\Seeder;
use App\Permission;
class PermissionSliderImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $person = new Permission([
            'name' => 'slider.image.view',
            'display_name' => 'slider image  view',
            'description' => 'Ver image de slider'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'slider.image.create',
            'display_name' => 'slider image create',
            'description' => 'Crear image de slider'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'slider.image.edit',
            'display_name' => 'slider image edit',
            'description' => 'Editar image de slider'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'slider.image.delete',
            'display_name' => 'slider image delete',
            'description' => 'Eliminar image de slider'
        ]);
        $person->save();
    }
}
