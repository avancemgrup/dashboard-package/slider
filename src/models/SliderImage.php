<?php

namespace Avannubo\Slider\Models;

use Illuminate\Database\Eloquent\Model;

class SliderImage extends Model{
    protected $fillable = [
        'id',
        'name',
        'route',
        'url',
        'description',
        'slider_id',
    ];
    public function slider(){
        return $this->belongsTo(Slider::class);
    }
}
