@extends('layouts.administration.master')

@section('site-title')
    Slider
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                <div class="row card__container">
                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                        @permission('slider.image.create')
                        <a href="{{ url('administration/slider/slider-images/add/'.$id) }}" class="btn btn-success">
                           Nuevo
                        </a>
                        @endpermission
                    </div>
                    <div class="col-md-offset-6 col-lg-offset-6 col-md-4 col-lg-4 col-xs-12 col-sm-12">
                        <div class="row end-md end-lg ">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                {!! Form::open(array('route' => ['slider-image',$id], 'method'=>'get')) !!}
                                 {!! Form::text('search', null, array('placeholder' => 'Buscar','class' => 'form-control', 'id' => 'search')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                <h3 class="table__name">Imagenes de slider</h3>
                @if (Session::has('error'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                {{ Session::get('error')  }}
                            </div>
                        </div>
                    </div>
                @endif
                @if (Session::has('message'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                {{ Session::get('message')  }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Nombre </th>
                            <th>Ruta </th>
                            <th>URL </th>
                            <th>Descripción</th>
                            @if(Entrust::can('slider.image.edit') || Entrust::can('slider.image.delete'))
                            <th>Opciones</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sliderImage as $sliderImg)
                            <tr>
                                {{--<td>--}}
                                    {{--<div class="checkbox">--}}
                                        {{--<label><input type="checkbox" name="insurances" data-id="{{ $sliderImg->id }}"></label>--}}
                                    {{--</div>--}}
                                <td>{{$sliderImg->name}}</td>
                                <td><img width="80px" class="img-responsive" src="{{ asset('storage/sliders/'.$sliderImg->route) }}" alt="Avatar {{$sliderImg->name}}"></td>
                                <td>{{$sliderImg->url}}</td>
                                <td class="table-ellipsis">{!!$sliderImg->description!!}</td>
                                <td>
                                    @permission('slider.image.edit')
                                    <a href="{{ url('administration/slider/'.$sliderImg->Slider->id.'/slider-images/'.$sliderImg->id) }}" class="btn btn-default btn-icon">
                                        <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                    </a>
                                    @endpermission
                                    @permission('slider.image.delete')
                                    {!! Form::open(array('route' => ['slider-delete-image', $sliderImg->id], 'method'=>'DELETE','style' => 'display:inline-block')) !!}
                                    <button type="submit" class="btn btn-danger btn-icon"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></button>
                                    {!! Form::close() !!}
                                    @endpermission
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <hr>
                </div>
                <div class="row middle-xs end-md end-lg">
                    {{ $sliderImage->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection