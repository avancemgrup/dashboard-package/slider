@extends('layouts.administration.master')

@section('site-title')
    Slider
@endsection
@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Crear nueva imagen</h2>
                </div>
                <div class="no-margin-left">
                    <a class="btn btn-primary" href="{{ route('slider-image',$id) }}">
                       Volver
                    </a>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(array('route' => ['slider-add-images', $id], 'method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">Nombre:</label>
                    {!! Form::text('name', null, array('placeholder' => 'Nombre','class' => 'form-control', 'id' => 'name')) !!}
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                </div>
                <div class="form-group {{ $errors->has('route') ? 'has-error' : '' }}">
                    <label for="route">Imagen</label>
                    {!! Form::file('route', array('accept' => 'image/*','class' => 'form-control','id' => 'route')) !!}
                    <span class="text-danger">{{ $errors->first('route') }}</span>
                </div>
                <div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
                    <label for="url">URL</label>
                    {!! Form::text('url', null, array('placeholder' => 'URL','class' => 'form-control', 'id' => 'url')) !!}
                    <span class="text-danger">{{ $errors->first('url') }}</span>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                    <label for="description">Descripción</label>
                    {!! Form::textarea('description', null, array('placeholder' => 'Descripción','class' => 'form-control', 'id' => 'description')) !!}
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                </div>
                <button type="submit" class="btn btn-success">
                    Crear
                </button>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset("js/tinymce/tinymce.min.js") }}"></script>
    <script src="{{ asset("js/tinymce/tinymce_editor.js") }}"></script>
    <script>
        editor_config.path_absolute = "{{ url('/') }}";
        editor_config.selector = "textarea[name=description]";
        tinymce.init(editor_config);
    </script>
@endsection