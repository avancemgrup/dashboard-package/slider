@extends('layouts.administration.master')

@section('site-title')
  Slider
@endsection

@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                <div class="row card__container">
                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12">
                        @permission('slider.create')
                        <a href="{{ route('slider-add') }}" class="btn btn-success">
                          Nuevo
                        </a>
                        @endpermission
                    </div>
                    <div class="col-md-offset-6 col-lg-offset-6 col-md-4 col-lg-4 col-xs-12 col-sm-12">
                        <div class="row end-md end-lg ">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                {!! Form::open(array('route' => ['slider'], 'method'=>'get')) !!}
                                     {!! Form::text('search', null, array('placeholder' => 'Buscar','class' => 'form-control', 'id' => 'search')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                <h3 class="table__name">Slider</h3>
                    @if (Session::has('error'))
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-danger">
                                    {{ Session::get('error')  }}
                                </div>
                            </div>
                        </div>
                    @endif
                    @if (Session::has('message'))
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-success">
                                    {{ Session::get('message')  }}
                                </div>
                            </div>
                        </div>
                    @endif
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Nombre </th>
                            <th>Descripción</th>
                            @if(Entrust::can('slider.edit') || Entrust::can('slider.delete'))
                            <th>Opciones</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sliders as $slider)
                            <tr>
                                <td>
                                    @if(Entrust::can('slider.image.*'))
                                    <a class="card__insurance-name" href="{{ url('administration/slider/'.$slider->id."/slider-images") }}">{{$slider->name}}</a>
                                    @else
                                    {{$slider->name}}
                                    @endif
                                </td>
                                <td class="table-ellipsis">{{$slider->description}}</td>
                                <td>
                                    @permission('slider.edit')
                                    <a href="{{ route('slider-edit',$slider->id) }}" class="btn btn-default btn-icon">
                                        <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                    </a>
                                    @endpermission
                                    @permission('slider.delete')
                                    {!! Form::open(array('route' => ['slider-delete', $slider->id], 'method'=>'DELETE','style' => 'display:inline-block')) !!}
                                    <button type="submit" class="btn btn-danger btn-icon"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></button>
                                    {!! Form::close() !!}
                                    @endpermission
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <hr>
                </div>
                <div class="row middle-xs end-md end-lg">
                    {{ $sliders->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection