<?php

namespace Avannubo\Slider\Controllers;

use Avannubo\Slider\Models\Slider;
use Avannubo\Slider\Models\SliderImage;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Http\Controllers\Controller;


class SliderController extends Controller{
    /**
     * @author: Obiang
     * @date: 28/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method show the index view
     */
    public function  slider(Request $request){
        $search = $request->input('search');
        $sliders = Slider::where('name','LIKE', '%'.$search.'%')->paginate(15)->appends('search', $search);
        return view('slider::slider', compact('sliders'));
    }

    /**
     * @author: Obiang
     * @date: 29/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method show all the image of slider
     */
    public function  sliderImg(Request $request, $id){
        $search = $request->input('search');
        $sliderImage = Slider::find($id);
        if($sliderImage){
            $sliderImage =$sliderImage->sliderImages()->paginate(15)->appends('search', $search);
          //  $sliderImage->paginate(2);
        }
        return view('slider::slider-images', compact('sliderImage','id'));
    }

    /**
     * @author: Obiang
     * @date: 29/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @description: This method show the form of create new slider
     */
    public function createSlider(){
        return view('slider::form-slider');
    }

    /**
     * @author: Obiang
     * @date: 29/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @description: This method show the form of create slider images
     */
    public function createSliderImages($id){
        return view('slider::form-slider-images', compact('id'));
    }


    /**
     * @author: Obiang
     * @date: 29/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if
     * they are correct it inserts slider into the database
     * else Shows error
     *
     */
    public function storeSlider(Request $request){
        // Validation
        $rules = [
            'name' => 'required|Max:255',
            'description' => 'required|Max:400',
        ];
        $this->validate($request, $rules);

        $sliders = new Slider([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
        ]);
        if($sliders->save()){
            return redirect()->route('slider')->with(
                'message', 'Slider creado correctamente'
            );
        }else{
            return redirect()->route('slider')->with(
                'error', 'Slider no creado correctamente, intentelo mas tarde'
            );
        }

    }


    /**
     * @author: Obiang
     * @date: 29/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if 
     * they are correct it inserts slider images into the database
     * else Shows error
     *
     */
    public function storeSliderImages(Request $request,$id){
        // Validation
        $slider = Slider::find($id);
        if($slider == null){
           Session::flash('error','Not found result');
           return redirect('administration/slider/'.$id.'/slider-images/add')->with(
               'error', 'Resultado no encontrado'
           );
        }
        $rules = [
            'name' => 'required|Max:255',
            'route' => 'required|image',
            'url' => 'nullable|url|Max:255',
            'description' => 'nullable|Max:400',
        ];
        $this->validate($request, $rules);

        $route = $request->file('route');
        $routeName = time(). '.' .$route->getClientOriginalExtension();
        Storage::putFileAs('public/sliders', new File($route), $routeName );

        $sliderImages = new SliderImage([
            'name' => $request->input('name'),
            'route' => $routeName,
            'url' => $request->input('url'),
            'description' => $request->input('description'),
        ]);

        if($slider->sliderImages()->save($sliderImages)){
            return redirect('administration/slider/'.$id.'/slider-images')->with(
                'message', 'Imagen creada correctamente'
            );
        }else{
            return redirect('administration/slider/'.$id.'/slider-images')->with(
                'message', 'Imagen no creada correctamente, intentelo mas tarde'
            );
        }
    }

    /**
     * @author: Obiang
     * @date: 28/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates if exist the slider in the data base 
     * and delete if no exist show error
     */
    public function deleteSlider ($id){
        // Validation
        $slider = Slider::find($id);
        if($slider){
            if($slider->delete()){
                return redirect()->route('slider')->with(
                    'message', 'Slider eliminado correctamente'
                );
            }else{
                return redirect()->route('slider')->with(
                    'error','Slider no eliminado correctamente, intentelo mas tarde'
                );
            }
        }
        return redirect()->route('slider')->with(
            'error','Slider no encontrado'
        );
    }

    /**
     * @author: Obiang
     * @date: 28/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates if exist the slider image in the 
     * data base and delete if no exist show error
     */
    public function deleteSliderImage ($id){
        // Validation
        $sliderImage = SliderImage::find($id);
        if($sliderImage){
            if($sliderImage->delete()){
                if (Storage::disk('public')->exists('/sliders/' . $sliderImage->route)) {
                    Storage::disk('public')->delete('/sliders/' . $sliderImage->route);
                    Session::flash('message','Imagen eliminada correctamente');
                }
            }else{
                Session::flash('error','Imagen no eliminada correctamente, intentelo mas tarde');
            }
        }else{
            Session::flash('error','Imagen no encontrada');
        }
        return redirect('administration/slider/'.$sliderImage->slider->id.'/slider-images')->with(
            'Imagen eliminada correctamente'
        );
    }

    /**
     * @author: Obiang
     * @date: 28/06/2017
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method validates the existence of the information in 
     * the database,if no it exists it redirects a sliders else show the form 
     * update slider
     */
    public function editSlider($id){
        $sliders = Slider::find($id);
        if($sliders){
            return view('slider::updateForm-slider',compact('sliders','id'));
        }
        return redirect()->route('slider')->with(
            'error','Slider no encontrado'
        );
    }


    /**
     * @author: Obiang
     * @date: 28/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if 
     * they are correct it update slider into the database
     * else Shows error
     */
    public function updateSlider(Request $request, $id){
        // Validation
        $slider = Slider::find($id);
        if($slider){
            $rules = [
                'name' => 'required|Max:255',
                'description' => 'required|Max:400',
            ];
            $this->validate($request, $rules);

            $slider->name = $request->input('name');
            $slider->description = $request->input('description');
           if($slider->update()){
               return redirect()->route('slider')->with(
                   'message', 'Slider actualizado correctamente'
               );
           }else{
               return redirect()->route('slider')->with(
                   'error', 'Slider no ctualizado correctamente, intentelo mas tarde'
               );
           }
        }
        return redirect()->route('slider')->with(
            'error', 'Slider no encontrado'
        );
    }

    /**
     * @author: Obiang
     * @date: 29/06/2017
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method validates the existence of the information in 
     * the database, if no it exists it redirects a sliders else show the form 
     * update slider image
     */
    public function editSliderImage($idSlider, $id){
        $slider = Slider::find($idSlider);
        if($slider){
            $sliderImage = $slider->sliderImages()->find($id);

            if($sliderImage){
                return view('slider::updateForm-slider-image',compact('sliderImage','id','idSlider'));
            }
            return redirect('administration/slider/'.$idSlider.'/slider-images')->with(
                'error','Image no encontrada'
            );
        }
        return redirect('slider')->with(
            'error','Slider no encontrado'
        );
    }


    /**
     * @author: Obiang
     * @date: 29/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if
     * they are correct it update slider image into the database
     * else Shows error
     */
    public function updateSliderImage(Request $request, $id){
        // Validation
        $sliderImage = SliderImage::find($id);
        if($sliderImage){
            $rules = [
                'name' => 'required|Max:255',
                'route' => 'nullable|image',
                'url' => 'nullable|url|Max:255',
                'description' => 'nullable|Max:400',
            ];
            $this->validate($request, $rules);
            if($request->file('route') != null){
                if (Storage::disk('public')->exists('/sliders/'.$sliderImage->route)){
                    Storage::disk('public')->delete('/sliders/'.$sliderImage->route);
                }
                $route = $request->file('route');
                $routeName = time(). '.' .$route->getClientOriginalExtension();
                Storage::putFileAs('public/sliders', new File($route), $routeName );
                $sliderImage->route = $routeName;
            }
            $sliderImage->name = $request->input('name');
            $sliderImage->description = $request->input('description');
            $sliderImage->url = $request->input('url');

            $sliderImage->update();
            if($sliderImage->update()){
                return redirect('administration/slider/'.$sliderImage->slider->id.'/slider-images')->with(
                    'message', 'Imagen actualizada correctamente'
                );
            }else{
                return redirect('administration/slider/'.$sliderImage->slider->id.'/slider-images')->with(
                    'error', 'Imagen no actualizada correctamente, intentelo mas tarde'
                );
            }
        }
        return redirect('administration/slider/'.$sliderImage->slider->id.'/slider-images')->with(
            'error', 'Imagen no encontrada'
        );
    }

}
