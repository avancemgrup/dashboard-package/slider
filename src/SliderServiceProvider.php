<?php

namespace Avannubo\Slider;

use Illuminate\Support\ServiceProvider;

class SliderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //routes
        include __DIR__.'/routes/routes.php';
        //models
        include __DIR__.'/models/Slider.php';
        include __DIR__.'/models/SliderImage.php';
        //view blade
        $this->loadViewsFrom(__DIR__.'/views', 'slider');
        //migration
        $this->loadMigrationsFrom(__DIR__.'/migrations');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Avannubo\Slider\Controllers\SliderController');
    }
}
