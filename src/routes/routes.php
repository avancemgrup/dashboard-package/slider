<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'administration'], function () {
    Route::get('slider',[
        'as' => 'slider',
        'uses' => '\Avannubo\Slider\Controllers\SliderController@slider',
        'middleware' => ['permission:slider.view|slider.create|slider.edit|slider.delete']
    ]);
    Route::get('slider/{id}/slider-images',[
        'as' => 'slider-image',
        'uses' => '\Avannubo\Slider\Controllers\SliderController@sliderImg',
        'middleware' => ['permission:slider.image.view|slider.image.create|slider.image.edit|slider.image.delete']
    ]);
    Route::get('slider/add',[
        'as' => 'slider-add',
        'uses' => '\Avannubo\Slider\Controllers\SliderController@createSlider',
        'middleware' => ['permission:slider.create']
    ]);
    Route::get('slider/slider-images/add/{id}',[
        'as' => 'slider-add-images',
        'uses' => '\Avannubo\Slider\Controllers\SliderController@createSliderImages',
        'middleware' => ['permission:slider.image.create']
    ]);
    Route::get('slider/{id}',[
        'as' => 'slider-edit',
        'uses' => '\Avannubo\Slider\Controllers\SliderController@editSlider',
        'middleware' => ['permission:slider.edit']
    ]);
    Route::get('slider/{idSlider}/slider-images/{id}',[
        'as' => 'slider-image-edit',
        'uses' => '\Avannubo\Slider\Controllers\SliderController@editSliderImage',
        'middleware' => ['permission:slider.image.edit']
    ]);
    Route::post('slider/add',[
        'as' => 'slider-add',
        'uses' => '\Avannubo\Slider\Controllers\SliderController@storeSlider',
        'middleware' => ['permission:slider.create']
    ]);
    Route::post('slider/{id}/slider-images/add',[
        'as' => 'slider-add-images',
        'uses' => '\Avannubo\Slider\Controllers\SliderController@storeSliderImages',
        'middleware' => ['permission:slider.image.create']
    ]);
    Route::put('slider/{id}',[
        'as' => 'slider-edit',
        'uses' => '\Avannubo\Slider\Controllers\SliderController@updateSlider',
        'middleware' => ['permission:slider.edit']
    ]);
    Route::put('slider/slider-images/{id}',[
        'as' => 'slider-edit-images',
        'uses' => '\Avannubo\Slider\Controllers\SliderController@updateSliderImage',
        'middleware' => ['permission:slider.image.edit']
    ]);
    Route::delete('slider/{id}',[
        'as' => 'slider-delete',
        'uses' => '\Avannubo\Slider\Controllers\SliderController@deleteSlider',
        'middleware' => ['permission:slider.delete']
    ]);
    Route::delete('slider/{id}/slider-images',[
        'as' => 'slider-delete-image',
        'uses' => '\Avannubo\Slider\Controllers\SliderController@deleteSliderImage',
        'middleware' => ['permission:slider.image.delete']
    ]);

});