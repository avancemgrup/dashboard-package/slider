## Avannubo package slider

Slider

## Install

* Config composer.json: 
    
    require: {
    
        ...
        
        "avannubo/slider": "dev-master"
    }
   
    "repositories": [
        
        ...
        
        {
            
                "type": "vcs",
                
                "https://gitlab.com/avancemgrup/dashboard-package/slider.git"
                
        }
            
    ]

* Install: `composer update`
* Add to Service Provider: `Avannubo\Slider\SliderServiceProvider::class`
* seeders: `php artisan db:seed --class=Avannubo\Slider\Seeds\PermissionSliderSeeder`
* seeders: `php artisan db:seed --class=Avannubo\Slider\Seeds\PermissionSliderImageSeeder`
